require_relative 'inicializando.rb' 
require_relative 'sorteando.rb'
class Adivinhar_numero
    attr_accessor :numero
    attr_accessor :venceu

    def initialize
        Inicializando.inicializar
        @numero=Sortear.sorte
        @venceu=false
    end

    def adivinhar(numero=0)
        if @numero==numero
            @venceu=true
            return "voce acertou"
        elsif @numero>numero
            return "seu valor está abaixo do valor correto"
        else 
            return "seu valor está acima do valor correto"
        end
    end
end


